# Json-To-Sql-Package
A simple, easy to use script in JS to convert json file to DDL and DML commands.<br>
You can use it if you have a big json file with data and you want to put it in some SQL DB like Oracle or Postgresql. 

# Getting Started
```bash
npm install -g json-to-sql.js
```

# Usage
```bash
json-to-sql <path-to-json-file>
```
The table name will be 't_' plus your json file name without the suffix.<br>
After you run it, notice two files are created, one for DDL command and one for all the DML commands.

# Additional Info

*  The types of the fileds in the json file will remain the same - number to number, text to text...
*  Text fileds by default will be converted to 256 characters text columns in the DDL file.
*  Quotes will not harm the sql code so don't worry.
*  Id fields in the json file ("id": 5) will automatically converted to primary key column in the DDL file.
*  If on json in the array has filed that other json doesn't have, there will be a column for it and it will be inserted by NULL value.
